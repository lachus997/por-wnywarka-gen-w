function filteddotplot = filtrdotplotter(dotplot,k,s)
filteddotplot = zeros (size(dotplot));

for i=1:(size(dotplot,1)-k+1)
    for j=1:(size(dotplot,2)-k+1)
        licz = 0;
        for l=0:(k-1)
        licz = licz + dotplot(i+l,j+l);        
        end
        if licz>(k-s-1)
        for l=0:(k-1)
        if filteddotplot(i+l,j+l) < 1
        filteddotplot(i+l,j+l) = filteddotplot(i+l,j+l) + dotplot(i+l,j+l);        
        end  
        end
        end
    end
end

end