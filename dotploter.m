function dotplot = dotploter(por1,por2)
 dotplot = zeros(length(por1),length(por2));
for i=1:length(por1)
   for j=1:length(por2)
      if por1(i)== por2(j)
          dotplot(i,j)=1;
      end
   end
end
end