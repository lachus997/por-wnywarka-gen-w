function FastaStru = Fastaparser(Identyfikator, Fasta)
remain = char(Fasta);
FastaStru = struct('Identyfikator',{},'Etykieta',{},'Sekwencja',{});
i = 0;
while ~isempty(remain)
    [token,remain] = strtok(remain,newline);
    if ~isempty(token) && token(1) == '>'
        i=i+1;
        FastaStru(i).Identyfikator=Identyfikator;
        FastaStru(i).Etykieta = token(2:end);
        FastaStru(i).Sekwencja = '';
    elseif i == 1
        FastaStru(1).Sekwencja = strcat(FastaStru(1).Sekwencja, token);
    end
end
FastaStru(1).Sekwencja = char(FastaStru(1).Sekwencja);
end